# ansible wordpress lemp Stack

## Goals
Create a wordpress site using NGINX and postgresql using ansible.
shortly after I wrote this, I discoverd it isn't supported. We will be using mariaDB instead.

## Setting up the playbook
An example inventory is included `ex-inventory.yml`
You will need to adjust the host in there

In `vars/defaul.yml` you will need to adjust the variables for your site.  
- TODO - make this just the default and make it so each host can have it's variables defined in a host_vars file

## running the playbook
`ansible-playbook -i inventory.yml playbook.yml --ask-become-pass`

## Support
You are on your own for now.

## Roadmap
[] set this up to run via some CI/CD tooling

## Acknowledgement
created by Patrick Palmersheim